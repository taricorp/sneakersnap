sneakernet-based incremental backups with btrfs and Snapper.

Sneakersnap allows you to make incremental backups of a btrfs filesystem to
another btrfs filesystem offline (requiring no interactive communication
between the backup source and destination). It is meant for remote backups
where you can regularly move small amounts of data (where the size of an
incremental version is the size of the changes to the source since the last
backup) between two machines that need not be network-connected.

For example, you may wish to keep a backup of your home computer on an external
disk and keep that disk at your office to guard against catastrophic failures
(for example, a fire at home). Taking that external disk home once a week or so
risks damage to the disk in transport, and you lose redundancy when the backup
is at home. You could keep two independent backups only one of which is ever
in transit, but this doubles cost. Sneakersnap allows you to transport
incremental shapshots between the locations on another (portable) disk which
need not be very large so you can use a cheaper or more robust medium for
transport.

For large backups or slow network connections where physical items or people
regularly move between locations, the sneakernet solution is more efficient,
and helps ensure your backup's integrity because some manual action is required
to update the backup and revisions are stored.

If you want online backups using btrfs, consider
[btrbk](https://github.com/digint/btrbk) instead.

# Prerequisites

Sneakersnap requires a recent Python interpreter (probably 3.5 or newer). It
also expects to be able to communicate with Snapper running on the source
machine to manage snapshots.

Any paths you want to back up should be btrfs subvolumes (which can be
snapshotted with `btrfs subvolume snapshot`) and have Snapper configs set up for
them.

You can install Sneakersnap with pip:

    pip install hg+https://bitbucket.org/tari/sneakersnap#egg=sneakersnap

# Usage

Assume a volume configured in Snapper called 'photos' that you want to back up.

## Seeding

First seed the backup. This currently requires the destination device be
connected to the source machine. In this instance, assume it is mounted at
/media/backup.

    sneakersnap stream photos /media/backup

This creates a read-only snapshot with snapper and copies it to
/media/backup/photos/n/snapshot, where n is the generation number of the backup
(which should be 1 for a new backup). You may now remove the backup disk and put
it somewhere safe.

Note that you will probably need to run as root both for access to Snapper's
dbus interface and for writing the cloned subvolume to the destination.

## Incrementals

Stuff.

# Limitations

Doesn't remove old snapshots?

Ack is completely manual right now.

Offline seeding not yet supported.

# Planned features

Monitoring disks to automatically back up when a traveler disk is connected and
apply deltas on the other end.
