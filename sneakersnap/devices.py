import dbus
import logging
import threading

# Ugh GLib. Introspection makes useful documentation impossible.
# I have no idea what this does, really.
try:
    from gi.repository import GObject
except ImportError:
    import gobject as GObject
from dbus.mainloop.glib import DBusGMainLoop
DBusGMainLoop(set_as_default=True)
GObject.threads_init()

log = logging.getLogger('sneakersnap.devices')

# Basically: watch for devices added or changed. If a device appears with
# recognized UUID (should be usage:filesystem, type:btrfs), mount it
# and start doing stuff with snapper and send/recv.
#
# When done sending an incremental to the device, we want to unmount
# it and notify in some fashion.
#
# We may want to directly support opening encrypted devices too.
# In that case look for devices with usage:crypto type:crypto_LUKS
# (again, with a known UUID) and somehow get the key to unlock
# the device. Then we'll get the filesystems on the device as
# normal. When closing a filesystem we can check the CryptoBackingDevice
# on it to see if we should also close the encryption container.

class DeviceMonitorThread(threading.Thread):
    def __init__(self, sinkq, sourceq, crypto_uuids, sink_uuids, source_uuids, **kwargs):
        super().__init__(**kwargs)

        # Mounted a sink volume (mountpoint, uuid, finished_cb)
        self.sinkq = sinkq
        # Mounted a source volume (mountpoint, uuid, finished_cb)
        self.sourceq = sourceq
        # Map encrypted device UUIDs to keys
        self.crypto_uuids = crypto_uuids
        # List of UUIDs of sink devices
        self.sink_uuids = sink_uuids
        # List of UUIDs of source devices
        self.source_uuids = source_uuids

        # Connect up a proxy (this will error out if the service isn't
        # available)
        self.bus = dbus.SystemBus()
        self.udisks = self.bus.get_object('org.freedesktop.UDisks', '/org/freedesktop/UDisks')

        # Listen to device changes
        # Other signals: DeviceRemoved, DeviceJobChanged
        self.udisks.connect_to_signal('DeviceAdded', self.on_change)
        #self.udisks.connect_to_signal('DeviceChanged', self.on_change)
        self.loop = GObject.MainLoop()

    def run(self):
        log.debug("Device monitor thread entering event loop")
        self.loop.run()

    def stop(self):
        self.loop.quit()

    def on_change(self, object_path):
        IDevice = 'org.freedesktop.UDisks.Device'
        log.debug('DBus add/change event for UDisks device ' + object_path)
        proxy = self.bus.get_object('org.freedesktop.UDisks', object_path)
        device = dbus.Interface(proxy, IDevice)
        props = dbus.Interface(proxy, 'org.freedesktop.DBus.Properties')

        uuid = props.Get(IDevice, 'IdUuid')
        usage = props.Get(IDevice, 'IdUsage')
        ty = props.Get(IDevice, 'IdType')
        if not uuid:
            log.info('Ignoring event; no UUID available')
            return

        if uuid in self.crypto_uuids:
            if usage != 'crypto' or ty != 'crypto_LUKS':
                log.error('Volume %s is configured for encryption but is not a LUKS volume', uuid)
                return
            log.debug("Unlocking encrypted device with known key")
            # What happens if this fails? No idea.
            device.LuksUnlock(self.crypto_uuids[uuid], [])

        elif uuid in self.sink_uuids:
            if usage != 'filesystem':
                log.error("Sink device %s does not appear to be a filesystem", uuid)
                return
            log.debug("Mounting sink volume %s", uuid)

            # Grab the backing device if this fs is on an encrypted device.
            is_encrypted = props.Get(IDevice, 'DeviceIsLuksCleartext')
            if is_encrypted:
                crypt_dev = props.Get(IDevice, 'LuksCleartextSlave')
                proxy = self.bus.get_object('org.freedesktop.UDisks', crypt_dev)
                crypt_dev = dbus.Interface(proxy, 'org.freedesktop.UDisks.Device')
            else:
                crypt_dev = None

            try:
                # Try to mount the filesystem
                mount_path = device.FilesystemMount(ty, [])
            except:
                # If failed to mount and is encrypted, re-lock the device.
                if crypt_dev is not None:
                    log.debug("Failed to mount encrypted volume; re-locking")
                    crypt_dev.LuksLock([])
                raise

            # Mounted okay; send out for processing
            def finished_evt():
                log.debug("Event handling for %s finished; unmounting", uuid)
                device.FilesystemUnmount([])
                if crypt_dev is not None:
                    log.debug("Locking encrypted device after unmount")
                    crypt_dev.LuksLock([])
            self.sinkq.put((mount_path, uuid, finished_evt))

        elif uuid in self.source_uuids:
            raise NotImplementedError("Source mode not yet implemented")

        else:
            log.info("Device %s (type:%s,usage:%s) appeared but is not known; ignoring",
                     uuid, ty, usage)
