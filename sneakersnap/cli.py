import argparse
import logging
import os.path
import subprocess
import queue
from configparser import SafeConfigParser

from sneakersnap.snapshot import Snapshot

CONFIG_FILE_PATH = 'config.ini'

log = logging.getLogger('sneakersnap.cli')

# We have two types of volumes: source and sink. A sink volume receives
# incrementals from snapshots for a specified snapper config, and a source
# volume reads incrementals for the given name to the specified path.
#
# On the source machine:
# [portable-disk-uuid]
# mode = sink
# configs = caring:caring,caring-incoming:caring-incoming
#
# And the destination machine:
# [portable-disk-uuid]
# mode = source
# configs = caring:/mnt/caring,caring-incoming:/mnt/caring-incoming
#
# For encrypted devices, specify crypto mode and a key or keyfile
# [portable-disk-uuid]
# mode = crypto
# crypt_passphrase = sup3rs3cur3
# # or: crypt_keyfile = /root/mykeyfile.dat

def main(argv, configure_logging=True):
    p = argparse.ArgumentParser(prog=argv[0])
    p.add_argument('--verbose', '-v', action='store_true')
    p.add_argument('--config-file', '-c', default='config.ini')
    subp = p.add_subparsers()

    p_send = subp.add_parser('send')
    p_send.add_argument('config')
    p_send.add_argument('dest_dir')
    p_send.set_defaults(func=command_send)

    p_receive = subp.add_parser('receive')
    p_receive.add_argument('source_file')
    p_receive.add_argument('dest_path')
    p_receive.add_argument('config_name')
    p_receive.add_argument('generation')
    p_receive.set_defaults(func=command_receive)

    p_ack = subp.add_parser('ack')
    p_ack.add_argument('config_name')
    p_ack.add_argument('generation', type=int)
    p_ack.add_argument('key')
    p_ack.set_defaults(func=command_ack)

    p_cleanup = subp.add_parser('cleanup')
    p_cleanup.add_argument('config_name')
    p_cleanup.set_defaults(func=command_cleanup)

    p_monitor = subp.add_parser('monitor')
    p_monitor.set_defaults(func=command_monitor)

    p_stream = subp.add_parser('stream')
    p_stream.add_argument('config_name')
    p_stream.add_argument('dest_path')
    p_stream.set_defaults(func=command_stream)

    options = p.parse_args(argv[1:])
    if configure_logging:
        if options.verbose:
            logging.basicConfig(level=logging.DEBUG)
        else:
            logging.basicConfig(level=logging.WARNING)

    config = SafeConfigParser()
    config.read(options.config_file)

    # Map UUID to encryption key
    crypto_keys = {}
    # Map UUID to .. something
    source_uuids = {}
    # Map UUID to snapper config name
    sink_uuids = {}

    for volume in config.sections():
        mode = config.get(volume, 'mode')
        if mode == 'sink':
            configs = config.get(volume, 'configs').split(',')
            sink_uuids[volume] = dict(map(lambda s: tuple(s.split(':')), configs))
        elif mode == 'source':
            raise NotImplementedError("Receive side not yet implemented")
        elif mode == 'crypto':
            if config.has_option(volume, 'crypt_passphrase'):
                key = config.get(volume, 'crypt_passphrase').encode()
            elif config.has_option(volume, 'crypt_keyfile'):
                with open(config.get(volume, 'crypt_keyfile'), 'rb') as f:
                    key = f.read()
            else:
                log.error("Volume %s in crypto mode has no key specified; ignoring", volume)
                continue
            crypto_keys[volume] = key
        else:
            log.error('Unrecognized volume mode: %s', mode)

    config = {
        'crypto_keys': crypto_keys,
        'sink_uuids': sink_uuids,
        'source_uuids': source_uuids,
    }
    return options.func(options, config)


def command_send(options, config):
    source_config = options.config
    dest_dir = options.dest_dir
    log.debug('Creating snapshot for config %s', source_config)
    snapshot = Snapshot.create(source_config)
    log.debug('Created snapshot #%i for config %s', snapshot.id, source_config)
    parent = snapshot.parent()
    if parent is not None:
        # Incremental
        dest_file = 'sneakersnap.{}.gen{}-{}.delta'.format(source_config,
                                                           parent.generation,
                                                           snapshot.generation)
        dest_file = os.path.join(dest_dir, dest_file)
        log.info('Doing incremental send for snapshot %i of config %s with \
parent %i to file %s',
                 snapshot.id, source_config, parent.id, dest_file)
        subp = _send_to_file(dest_file, snapshot, parent)
        subp.wait()
        if subp.returncode != 0:
            raise Exception('btrfs send failed')
        # Update key with size of delta, used for ack confirmation
        snapshot.key = os.stat(dest_file).st_size
        snapshot.update_userdata()
    else:
        # Non-incremental; no acked parent
        raise NotImplementedError('Non-incremental send not implemented')


def command_receive(options, config):
    source_file = options.source_file
    dest_path = options.dest_path
    config_name = options.config_name
    generation = options.generation

    # Create directory for output generation
    dest_path = os.path.join(dest_path, config_name, str(generation))
    log.debug('Creating destination path %s', dest_path)
    os.makedirs(dest_path)

    # Do the receive
    key = os.stat(source_file).st_size
    subp = _receive_from_file(source_file, dest_path)
    subp.wait()
    if subp.returncode != 0:
        raise Exception('btrfs receive failed')

    # Success. Remove input file and say we're done
    log.debug('Receive complete, removing input file %s', source_file)
    os.unlink(source_file)

    print('Receive complete to', dest_path)
    print('Ack key:', key)


def command_ack(options, config):
    config_name = options.config_name
    generation = options.generation
    key = options.key

    source = Snapshot.get_generation(config_name, generation)
    if source is None:
        print('Generation', generation, 'for config', config_name,
              'does not exist!')
        return 1
    if source.key != key:
        print('Specified key does not match generation', generation)
        return 1
    source.ack = True
    source.update_userdata()
    print('Acknowledged reception of generation', generation,
          'for volume', config_name)


def command_cleanup(options, config):
    config_name = options.config_name
    # Clean up generations which have been acked, but keep the single newest
    # generation which has been acked, since it's the seed for the next send.
    pass


def command_monitor(options, config):
    from sneakersnap.devices import DeviceMonitorThread
    from sneakersnap.snapshot import SnapshotCreationThread

    sinkq = queue.Queue()
    sourceq = queue.Queue()
    monitor = DeviceMonitorThread(sinkq, sourceq, crypto_keys,
                                  list(sink_uuids.keys()),
                                  list(source_uuids.keys()))
    snapper = SnapshotCreationThread(sinkq)

    monitor.start()
    snapper.start()

def command_stream(options, config):
    source_config = options.config_name
    dest_path = options.dest_path
    snapshot = Snapshot.create(source_config)

    try:
        pipe_path = '/tmp/sneakersnap-stream.{}.fifo'.format(os.getpid())
        os.mkfifo(pipe_path, mode=0o600)
        log.debug('Streaming through %s', pipe_path)
        sender = _send_to_file(pipe_path, snapshot, snapshot.parent())

        dest_path = os.path.join(dest_path, source_config, str(snapshot.generation))
        log.debug('Creating destination path %s', dest_path)
        os.makedirs(dest_path)
        receiver = _receive_from_file(pipe_path, dest_path)

        sender.wait()
        receiver.wait()

        # Mark as completed.
        snapshot.key = -1
        snapshot.ack = True
        snapshot.update_userdata()
    finally:
        os.unlink(pipe_path)


def _send_to_file(dest_path, snapshot, parent=None):
    parent_fragment = []
    if parent is not None:
        parent_fragment = ['-p', parent.path()]

    return subprocess.Popen(['btrfs', 'send'] + parent_fragment +
            ['-f', dest_path, snapshot.path()])


def _receive_from_file(source_file, dest_path):
    log.debug('Invoking btrfs receive from %s to %s', source_file, dest_path)
    return subprocess.Popen(['btrfs', 'receive', '-f', source_file, dest_path])

