import dbus
import logging
import threading
from datetime import datetime

log = logging.getLogger('sneakersnap.snapshot')

class SnapshotCreationThread(threading.Thread):
    def __init__(self, queue, **kwargs):
        super().__init__(**kwargs)
        self.daemon = True

        self.queue = queue

    def run(self):
        while True:
            (mountpoint, uuid, finished_cb) = self.queue.get()
            log.info('Sink volume %s mounted; time to send to it', uuid)
            # Nothing implemented here. Just pretend it finished.
            finished_cb()


class Snapshot(object):
    _bus = dbus.SystemBus()

    @classmethod
    def _connect_bus(cls):
        proxy = cls._bus.get_object('org.opensuse.Snapper', '/')
        return dbus.Interface(proxy, 'org.opensuse.Snapper')

    def __init__(self, config_name, id, _type, _uid, timestamp, _pre, description, _cleanup, userdata):
        self.config_name = config_name
        self.id = id
        self.timestamp = datetime.utcfromtimestamp(timestamp)
        self.description = description
        self.userdata = userdata

    def __repr__(self):
        return '<Snapshot #{}, dated {} - desc "{}", generation {} ack={}>'.format(
                self.id, self.timestamp.isoformat(), self.description,
                self.generation, self.ack
        )

    @property
    def generation(self):
        return int(self.userdata.get('generation', '-1'))
    @generation.setter
    def generation(self, value):
        self.userdata['generation'] = str(value)

    @property
    def ack(self):
        return bool(int(self.userdata.get('ack', '0')))
    @ack.setter
    def ack(self, value):
        self.userdata['ack'] = str(int(value))

    @property
    def key(self):
        return self.userdata.get('key', None)
    @key.setter
    def key(self, value):
        self.userdata['key'] = str(value)

    @classmethod
    def from_tuple(cls, config_name, tup):
        return cls(config_name, *tup)

    @classmethod
    def create(cls, config_name: str):
        max_gen = cls.max_generation(config_name) or 0
        snapper = cls._connect_bus()
        num = snapper.CreateSingleSnapshotV2(config_name,
                0, True, 'sneakersnap', '',
                {'generation': str(max_gen + 1),
                 'ack': str(int(False))}
        )
        return cls.get(config_name, num)

    @classmethod
    def get(cls, config_name, num):
        snapper = cls._connect_bus()
        return cls(config_name, *snapper.GetSnapshot(config_name, num))

    @classmethod
    def get_generation(cls, config_name, generation):
        for snapshot in cls.list(config_name):
            if snapshot.generation == generation:
                return snapshot
        else:
            return None

    @classmethod
    def list(cls, config_name: str, predicate=lambda s: True):
        snapper = cls._connect_bus()
        return filter(lambda s: s.description != 'current' and predicate(s),
                      map(lambda s: cls(config_name, *s),
                          snapper.ListSnapshots(config_name)))

    @classmethod
    def max_generation(cls, config_name: str, predicate=lambda s: True) -> int:
        try:
            return max(int(s.generation)
                       for s in cls.list(config_name)
                       if s.description == 'sneakersnap' and predicate(s))
        except ValueError:
            return None

    def update_userdata(self):
        snapper = self._connect_bus()
        snapper.SetSnapshot(self.config_name, self.id, self.description,
                            '', self.userdata)

    def path(self) -> str:
        snapper = self._connect_bus()
        return snapper.GetMountPoint(self.config_name, self.id)

    def parent(self, acked=True):
        def is_older(snap):
            match = True
            match &= snap.generation < self.generation
            if acked:
                match &= snap.ack
            return match
        parent_gen = self.max_generation(self.config_name, predicate=is_older)
        if parent_gen is None:
            return None
        else:
            return self.get_generation(self.config_name, parent_gen)


if __name__ == '__main__':
    print(list(Snapshot.list('caring')))
    print('Max generation is', Snapshot.max_generation('caring'))
    print('..creating snapshot')
    snap = Snapshot.create('caring')
    print(snap)
    print('New snapshot is mounted at', snap.path())
    print('its parent is mounted at', snap.parent().path())
