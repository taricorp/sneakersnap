from setuptools import setup

setup(name='sneakersnap',
      version='0.1.0',
      description='An incremental backups tool for the sneakernet',
      packages=['sneakersnap'],
      zip_safe=True,

      install_requires = [
          'dbus-python',
          'pygobject'
      ],

      entry_points={
          'console_scripts': [
              'sneakersnap = sneakersnap:script_run',
          ],
      },
)
